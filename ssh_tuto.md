# Tutorial para crear llaves y realizar conexión SSH
Para realizar una conexión SSH es necesario contar con credenciales que nos servirán para identificarnos. Para esto se necesita un par de "llaves". Una de ellas se le denomina pública (cuenta con la extensión .pub) que es la que se otorgará al servidor remoto al cual queremos acceder; la otra llave se le conoce como privada (usualmente con terminación .key). Ésta última es la que se conserva en nuestro equipo y que en su momento se hará referencia para comprobar que seamos los usuarios permitidos.

Para lograr este proceso de creación y posteriormente de reconocimiento por el server se recomienda seguir los siguientes pasos. Pueden haber diferente métodos, pero este se me hizo particularmente amigable. Este proceso ha sido probado ya en varias ocasiones con sistema operativo Windows (versión 10 pero debe ser practicamente igual para cualquier equipo.)

## Windows
### 1. DESCARGAR E INSTALAR PUTTY 

>Descarga la última versión a través del siguiente enlace: [Descarga PUTTY] (https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html).

En mi caso que utilizo Windows de 64 bits elegí la versión `putty-64bit-0.70-installer.msi`. Realiza la descarga y sigue los pasos de inStalación.

### 2. GENERACIÓN DE LLAVES
* Abrir la aplicación PuttyGen
* En la parte inferior aparecen las opciones de parámetros, aquí podemos personalizar el formato de nuestra llave, en mi casa fue suficiente dejar los datos tal y como están por default (Type: RSA, Bits: 2048). Finalmente presionamos en la opción `Generate`.
* Se nos pedirá por la aplicación que deslicemos el mouse en un área. Realizamos esta acción hasta que la barra de proceso termine.
* Al terminar de cargar la barra de proceso nos apareceran una campos de opciones para personalizar le llave. Para el caso de **Key comment** debemos ingresar un comentario para poder identificar nuestra llave, en mi caso puse la marce de mi laptop para ayudarme a distinguir entre mis equipos. Recomiendo sea una palabra sencilla. Para la opción **Key passphrase** (es igual a contraseña) es posible dejar este espacio en blanco,recomiendo que este campo sea llenado ya que dará aún más seguridad a la llave. De igual manera es recomendable apuntar estos datos ya que al configurar el **passphrase** se nos pedirá ingresarlo cada vez que realicemos una conexión.
* Porteriormente presionamos las dos opciones `Save public key` y `Save private key`. En ambas situaciones se nos pedirá que especifiquemos el lugar donde queremos guardar. Tomar nota de donde se realiza el guardado. 
* Finalmente es necesario realizar un paso más. En la misma aplicación seleccionamos la opción `Conversions` y luego la opción `Export Open SSH Key` y guardamos nuestra llave con nuevo formato. Esto debido a que la mayoría de los servidor usan OpenSSH y de no realizar esta opción habrán problemas con el formato de la llave.


### 3. ENTREGA DE LLAVE PÚBLICA
Al finalizar la generación de llaves, la pública se entregará al administador del server al cual queremos acceder. 
`* Para repositorios:` La cadena que debe ingresarse para la conexión ssh debe ser la que aparece en el recuadro  de la aplicación de PuttyGen "Public key for pasting into OpenSSh authorized _keys file.

### 3.1 (Opcional Reomendado) ARCHIVO CONFIG. 
Es común solicitar acceso a diferentes servidores, para esto siempre hay que ingresar línea el usuario, la ip y contraseña respectiva. Para evitar tener que ingresar estos datos cada vez que se solicita el ingreso al servidor es muy útil crear un archivo de configuraciones que contenga los datos de conexión. Este archivo debe localizarse en la raíz de la carpeta .ssh. La estructura del archivo debe ser la siguiente 

`Host 'alias Del Host'`

`User 'tu Usuario'`

`Port ''your port` (optional)

`HostName 'ip Del Host'`

`IdentityFile 'path De Llave Privada'`

 
 Explicando a detalles:
 >**Host**: Es el nombre con el cual identificarás al servidor, como ejemplo podría utilizarse el nombre del dominio o del proyecto al cual hace referencia.

 >**User**: Es el usuario con el cual te concedieron acceso al servidor. Usualmente el administrador de sistemas debe proporcionarte este dato.

  >**Port**: Es el puerto en el cual se ha configurado el proyecto (por default es el 22).

 >**HostName**: Ip del servidor (puede ser la DNS a la que pertenece la IP)

 >**IdentityFile**: Es el path en el que se encuentra la llave privada generada. Usualmente localizada en la carpeta .ssh.



 ### 4. Realizando conexión (con archivo config)
 Para realizar la conexión basta con escribir el siguiente comando vía *git bash*:

 `$ ssh 'Host'(Definido previamente en el archivo config) `









